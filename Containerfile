FROM docker.io/gitpod/openvscode-server:1.66.1

ENTRYPOINT ${OPENVSCODE_SERVER_ROOT}/entrypoint.sh

USER root

# live share requirements
RUN ln -s -f /bin/true /usr/bin/chfn \
    && wget -O ~/vsls-reqs https://aka.ms/vsls-linux-prereq-script \
    && chmod +x ~/vsls-reqs \
    && ~/vsls-reqs

# rust
# https://github.com/rust-lang/docker-rust/blob/682bb532b4a3ba9f9d57f5a1ba3cb4ddd51fd127/1.57.0/buster/Dockerfile
ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH \
    RUST_VERSION=1.60.0

RUN apt update -y \
    && apt install -y gcc libssl-dev pkg-config \
    && wget https://static.rust-lang.org/rustup/archive/1.24.3/x86_64-unknown-linux-gnu/rustup-init \
    && chmod +x rustup-init \
    && mkdir $RUSTUP_HOME $CARGO_HOME \
    && chmod -R a+w $RUSTUP_HOME $CARGO_HOME

# install fish
ENV OMF_CONFIG=/usr/share/fish/omf

RUN apt install -y fish curl htop pass \
    && curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install > install \
    && XDG_CONFIG_HOME=/etc fish install --path=/etc/omf --config=$OMF_CONFIG --noninteractive --verbose && usermod --shell /usr/bin/fish openvscode-server \
    && rm install \
    && fish -c 'omf install agnoster' \
    && usermod --shell /usr/bin/fish root \
    && fish -c 'omf doctor'

COPY gpg.fish /etc/fish/conf.d/

# install nodejs
RUN curl -sL https://deb.nodesource.com/setup_16.x -o ${OPENVSCODE_SERVER_ROOT}/nodesource_setup.sh \
    && bash ${OPENVSCODE_SERVER_ROOT}/nodesource_setup.sh \
    && apt install -y nodejs \
    && node --version

# install gcm
RUN wget https://github.com/GitCredentialManager/git-credential-manager/releases/download/v2.0.696/gcmcore-linux_amd64.2.0.696.deb -O /root/gcmcore-linux_amd64.2.0.696.deb \
    && dpkg -i /root/gcmcore-linux_amd64.2.0.696.deb

RUN chown -R openvscode-server:openvscode-server ~/.config \
    && chown -R openvscode-server:openvscode-server ~/.cache

USER openvscode-server

RUN ./rustup-init -y --no-modify-path --profile minimal --default-toolchain $RUST_VERSION --default-host x86_64-unknown-linux-gnu \
    && rm rustup-init \
    && rustup component add \
        rust-src \
        rustfmt \
        clippy \
    && cargo install cargo-edit cargo-expand cargo-watch ripgrep exa bat \
    && rustup --version \
    && cargo --version \
    && rustc --version

COPY --chown=openvscode-server \
     entrypoint.sh \
     settings.json \
     MS-vsliveshare.vsliveshare.vsix \
     MS-vsliveshare.vsliveshare-pack-0.4.0.vsix \
     vscode-org-mode.org-mode-1.0.0.vsix \
     rust-analyzer-linux-x64.vsix \
     config.fish \
     ${OPENVSCODE_SERVER_ROOT}/
