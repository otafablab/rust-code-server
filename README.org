* ~rust-code-server~

[[screenshot.png]]

Rust code server is the official VSCode OCI image used in the [[https://gitlab.com/otafablab/rust-lukiokurssi][Rust high school course]].

In a production environment, the servers can be orchestrated using [[https://gitlab.com/otafablab/rcs-backend][rust code server backend]]

** Features

 - Fully customizable VSCode
 - Chrome browser supported (not firefox)
 - Rich color theme optimized for high performance rust programming
 - Rust, git and org-mode plugins ready to rock!

** Usage

#+BEGIN_SRC sh
podman run --rm -p 3000:3000 \
  -v gitpod-vscode:/home/workspace \
  -v $PWD:/mnt:Z \
  registry.gitlab.io/otafablab/rust-code-server/rust-code-server
#+END_SRC

Your ~rust-code-server~ instance is available at http://localhost:3000/

*** Capabilities

Useful capabilities to provide for the container:

 - `CAP_AUDIT_WRITE`: allows use of sudo
 - `CAP_SYS_PTRACE`: allows use of strace

** Manual Building

The following commands can be used to create a podman image
~rust-code-server~ which has all the goodies installed

#+BEGIN_SRC sh
podman build . -t rust-code-server
#+END_SRC

Now, to start the server, run the following commands

#+BEGIN_SRC sh
podman run --rm -it -p 3000:3000 -v gitpod-vscode:/home/workspace -v $PWD:/mnt:Z --cap-add=SYS_PTRACE rust-code-server
#+END_SRC

** Deleting

#+BEGIN_SRC sh
podman volume rm -f gitpod-vscode
#+END_SRC

** Logo

[[rust-code-server.png]]
