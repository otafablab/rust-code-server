#!/bin/sh -x

BIN=${OPENVSCODE_SERVER_ROOT}/bin/openvscode-server

# install extensions to vscode on every time server is started
# TODO only install once?
$BIN --install-extension vadimcn.vscode-lldb@1.7.0
$BIN --install-extension dracula-theme.theme-dracula@2.24.2
$BIN --install-extension mhutchie.git-graph@1.30.0
$BIN --install-extension eamodio.gitlens@12.0.6
$BIN --install-extension usernamehw.errorlens@3.4.2
$BIN --install-extension stkb.rewrap@1.61.3
$BIN --install-extension bungcip.better-toml@0.3.2

$BIN --install-extension ${OPENVSCODE_SERVER_ROOT}/MS-vsliveshare.vsliveshare.vsix
$BIN --install-extension ${OPENVSCODE_SERVER_ROOT}/MS-vsliveshare.vsliveshare-pack-0.4.0.vsix
$BIN --install-extension ${OPENVSCODE_SERVER_ROOT}/vscode-org-mode.org-mode-1.0.0.vsix
# TODO install rust-analyzer from open vsx once it's up to
# date. update: open-vsx marketplace still has v0.2.853
$BIN --install-extension ${OPENVSCODE_SERVER_ROOT}/rust-analyzer-linux-x64.vsix

$BIN --list-extensions --show-versions

cp -v ${OPENVSCODE_SERVER_ROOT}/settings.json ~/.openvscode-server/data/Machine/

chown -R openvscode-server:openvscode-server ${OPENVSCODE_SERVER_ROOT}

cp -v ${OPENVSCODE_SERVER_ROOT}/config.fish ~/.config/fish/

exec $BIN --without-connection-token --port 3000 --host 0.0.0.0 "${@}" --
