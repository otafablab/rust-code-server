if status --is-interactive
    set -x GCM_CREDENTIAL_STORE gpg
    set -x GPG_TTY (tty)
end
